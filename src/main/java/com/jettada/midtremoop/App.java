package com.jettada.midtremoop;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        int number;
        Notebook Asus = new Notebook("Asus", 30000, 5);
        Notebook Acer = new Notebook("Acer", 23000, 2);
        Notebook Lenovo = new Notebook("Lenovo", 20000, 1);
        Notebook Dell = new Notebook("Dell", 20000, 5);
        Notebook Hp = new Notebook("Hp", 20000, 2);
        Notebook Huawei = new Notebook("Huawei", 20000, 1);
        Gaminggear Mouse = new Gaminggear("Mouse", 2000, 5);
        Gaminggear Keyboard = new Gaminggear("Keyboard", 5000, 5);
        Gaminggear Headphone = new Gaminggear("Headphone", 1500, 5);
        Asus.showProduct();
        Acer.showProduct();
        Lenovo.showProduct();
        Dell.showProduct();
        Hp.showProduct();
        Huawei.showProduct();
        System.out.print("brand: ");
        String name = kb.next();
        switch (name) {
            case "Asus":
                System.out.print("Get amount: ");
                number = kb.nextInt();
                Asus.buy(number);
                System.out.println("-----Check Stock-----");
                Asus.Stock(number);
                System.out.println(Asus);
                break;
            case "Acer":
                System.out.print("Get amount: ");
                number = kb.nextInt();
                Acer.buy(number);
                System.out.println("-----Check Stock-----");
                Acer.Stock(number);
                System.out.println(Acer);
                break;
            case "Lenovo":
                System.out.print("Get amount: ");
                number = kb.nextInt();
                Lenovo.buy(number);
                System.out.println("-----Check Stock-----");
                Lenovo.Stock(number);
                System.out.println(Lenovo);
                break;
            case "Dell":
                System.out.print("Get amount: ");
                number = kb.nextInt();
                Dell.buy(number);
                System.out.println("-----Check Stock-----");
                Dell.Stock(number);
                System.out.println(Dell);
                break;
            case "Hp":
                System.out.print("Get amount: ");
                number = kb.nextInt();
                Hp.buy(number);
                System.out.println("-----Check Stock-----");
                Hp.Stock(number);
                System.out.println(Hp);
                break;
            case "Huawei":
                System.out.print("Get amount: ");
                number = kb.nextInt();
                Huawei.buy(number);
                System.out.println("-----Check Stock-----");
                Huawei.Stock(number);
                System.out.println(Huawei);
                break;
            default:
                System.out.println("-----Error-----");
                System.out.println("-----Error-----");
                System.out.println("-----Error-----");
                System.out.println("-----Error-----");
                System.out.println("-----Error-----");
        }
        Mouse.showProduct();
        Keyboard.showProduct();
        Headphone.showProduct();
        while (true) {
            System.out.print("Gaming Gear: ");
            String gaminggear = kb.next();
            switch (gaminggear) {
                case "Mouse":
                    System.out.print("Get amount: ");
                    number = kb.nextInt();
                    Mouse.buy(number);
                    System.out.println("-----Check Stock-----");
                    Mouse.Stock(number);
                    System.out.println(Mouse);
                    break;
                case "Keyboard":
                    System.out.print("Get amount: ");
                    number = kb.nextInt();
                    Keyboard.buy(number);
                    System.out.println("-----Check Stock-----");
                    Keyboard.Stock(number);
                    System.out.println(Keyboard);
                    break;
                case "Headphone":
                    System.out.print("Get amount: ");
                    number = kb.nextInt();
                    Headphone.buy(number);
                    System.out.println("-----Check Stock-----");
                    Headphone.Stock(number);
                    System.out.println(Headphone);
                    break;
                case "No":
                    System.out.println("-----Thank You-----");
                    System.exit(0);
                    break;
                default:
                    System.out.println("-----Error-----");
                    System.out.println("-----Error-----");
                    System.out.println("-----Error-----");
                    System.out.println("-----Error-----");
                    System.out.println("-----Error-----");
                    break;
            }
        }
    }
}
