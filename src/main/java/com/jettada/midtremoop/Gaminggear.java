package com.jettada.midtremoop;

public class Gaminggear {
    private String name;
    private int money;
    private int number;
    private int buy;
    private int stock = 0;

    public Gaminggear(String name, int money, int number) {
        this.name = name;
        this.money = money;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }

    public int getNumber() {
        return number;
    }

    public int getBuy() {
        return buy;
    }

    public void showProduct() {
        System.out.println("brand: " + name + " | money: " + money + " | Stock: " + number);
    }

    public void buy(int num) {
        if (number - num < stock) {
            System.out.println("-----Out of Stock-----");
            num = num * 0;
            buy = money * 0;
            System.out.println("name: " + name + " buy: " + buy + " number: " + num);
        } else {
            buy = num * money;
            System.out.println("name: " + name + " buy: " + buy + " number: " + num);
        }
    }

    public String toString() {
        return "name: " + name + "|Stock: " + stock;
    }

    public int Stock(int num) {
        if (number - num < stock) {
            return stock = number;
        } else {
            return stock = number - num;
        }
    }

}
